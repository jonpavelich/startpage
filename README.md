# Startpage
## What is this?
A fast, tiny, personalized start page you can easily customize. It is built using [Hugo](https://gohugo.io). A demo is available at [https://start.jnpvl.ch](https://start.jnpvl.ch). You can use this project to easily create your own.

## Why?
I wanted my own start page so I can access my frequently used links even when away from my own computer. It is also tiny (~30KB before compression and only needs two requests), so it loads really quickly and is completely cacheable. I have it set as the homepage on all my devices, so my quick links are easily available anywhere.

## Deploying Your Own
1. Clone this repository (`git clone https://gitlab.com/jonpavelich/startpage.git`)
1. Install Hugo (see the [Hugo Documentation](https://gohugo.io/getting-started/installing/))
1. In `config.toml`, set `baseURL` to your own domain and edit the `[menu]` section to contain your own links. See [Configuring Links](#configuring-links) below.
1. Build the site using `hugo --minify` (or just `hugo` if you don't want the HTML minified).
1. Copy the generated files from `public/` to your web host

Any time you want to make changes, just re-run `hugo --minify` and copy the new files over

## Configuring Links
The basic format for creating sections is:
```
[menu]

    [[menu.section]]
    identifier = "section-1"
    name = "Section One"
    weight = 10

    	[[menu.section]]
	parent = "section-1"
	name = "My First Link"
	url = "https://example.com/link1.html"

	[[menu.section]]
	parent = "section-1"
	name = "My Second Link"
	url = "https://example.com/link2.html"

    [[menu.section]]
    identifier = "section-2"
    name = "Section Two"
    weight = 20
    
    	[[menu.section]]
	parent = "section-2"
	name = "My Third Link"
	url = "https://example.com/link3.html"

	...
```

- The `weight` parameter on each section orders them (lowest to highest).
- The `parent` parameter on each link needs to match the `identifier` parameter of a section, and nests links under that section.
- The links are sorted alphabetically (uppercase then lowercase) by default, but you can add `weight` parameters to them to sort in whatever order you'd like.

## Further Customization
You can look around `themes/startpage/`, which is where the magic happens. You can override any of the files there by copying them to the root of the repository (so for example, `themes/startpage/layouts/index.html` is overriden by `layouts/index.html` if it exists). You could also just edit them directly, but that makes pulling updates in a lot more challenging.

If you want to change the search engine used, you can override `themes/startpage/layouts/index.html` and change the form action. Before you do though, please read more about [why search engine privacy matters](https://duckduckgo.com/spread)!
